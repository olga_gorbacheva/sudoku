package ru.god.sum;

import java.io.IOException;
import java.io.*;
import java.util.ArrayList;

/**
 * Класс, с помощью которого можно решить ЛЮБОЕ судоку 4*4, которое гарантировано имеет решение
 * Не верите - проверьте!
 *
 * @author Горбачева, 16ИТ18к
 */
public class Solution {
    /**
     * Табличка самого судоку
     */
    private static char[] table = readArray();
    /**
     * Массивчик с точками, которые символизируют пустые ячейки
     */
    private static ArrayList<Integer> arrayOfPoints = new ArrayList<>();
    /**
     * Весьма необычный массивчик, который отражает количество заполненных ячеек
     * в строке/столбце/квадратике и сумму ячеек этого элемента соответственно
     */
    private static int[][] sum = new int[12][2];

    public static void main(String[] args) {
        calcSum();
        while (!arrayOfPoints.isEmpty()) {
            testByLines();
            testByColumns();
            testBySquares();
        }
        writeResult();
    }

    /**
     * Метод, который считывает исходные данные из файла
     *
     * @return символьный массив, в который построчно записана исходная таблица
     */
    private static char[] readArray() {
        char[] array = new char[16];
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\ru\\god\\sum\\input.txt"))) {
            String string;
            for (int index = 0; index < 16; ) {
                string = bufferedReader.readLine();
                for (int i = 0; i < 4; i++) {
                    array[index] = string.charAt(i);
                    index++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return array;
    }

    /**
     * Метод, который высчитывает количество заполненных элементов и их сумму
     * для каждой строки, столбца и квадратика соответственно
     */
    private static void calcSum() {
        int cell = 0;
        for (int value : table) {
            switch (value) {
                case '1': {
                    sum[line(cell)][0]++;
                    sum[line(cell)][1] += 1;
                    sum[column(cell)][0]++;
                    sum[column(cell)][1] += 1;
                    sum[square(cell)][0]++;
                    sum[square(cell)][1] += 1;
                    cell++;
                    break;
                }
                case '2': {
                    sum[line(cell)][0]++;
                    sum[line(cell)][1] += 2;
                    sum[column(cell)][0]++;
                    sum[column(cell)][1] += 2;
                    sum[square(cell)][0]++;
                    sum[square(cell)][1] += 2;
                    cell++;
                    break;
                }
                case '3': {
                    sum[line(cell)][0]++;
                    sum[line(cell)][1] += 3;
                    sum[column(cell)][0]++;
                    sum[column(cell)][1] += 3;
                    sum[square(cell)][0]++;
                    sum[square(cell)][1] += 3;
                    cell++;
                    break;
                }
                case '4': {
                    sum[line(cell)][0]++;
                    sum[line(cell)][1] += 4;
                    sum[column(cell)][0]++;
                    sum[column(cell)][1] += 4;
                    sum[square(cell)][0]++;
                    sum[square(cell)][1] += 4;
                    cell++;
                    break;
                }
                case '.': {
                    arrayOfPoints.add(cell++);
                    break;
                }
            }
        }
    }

    /**
     * Метод, определяющий, к какому квадрату относится ячейка
     *
     * @param cell - номер ячейки
     * @return номер квадрата
     */
    private static int square(int cell) {
        switch (cell) {
            case 0:
            case 1:
            case 4:
            case 5: {
                return 8;
            }
            case 2:
            case 3:
            case 6:
            case 7: {
                return 9;
            }
            case 8:
            case 9:
            case 12:
            case 13: {
                return 10;
            }
            case 10:
            case 11:
            case 14:
            case 15: {
                return 11;
            }
        }
        return 0;
    }

    /**
     * Метод, определяющий, к какому столбцу относится ячейка
     *
     * @param cell - номер ячейки
     * @return номер столбца
     */
    private static int column(int cell) {
        switch (cell) {
            case 0:
            case 4:
            case 8:
            case 12: {
                return 4;
            }
            case 1:
            case 5:
            case 9:
            case 13: {
                return 5;
            }
            case 2:
            case 6:
            case 10:
            case 14: {
                return 6;
            }
            case 3:
            case 7:
            case 11:
            case 15: {
                return 7;
            }
        }
        return 0;
    }

    /**
     * Метод, определяющий, к какой строке относится ячейка
     *
     * @param cell - номер ячейки
     * @return номер строки
     */
    private static int line(int cell) {
        switch (cell) {
            case 0:
            case 1:
            case 2:
            case 3: {
                return 0;
            }
            case 4:
            case 5:
            case 6:
            case 7: {
                return 1;
            }
            case 8:
            case 9:
            case 10:
            case 11: {
                return 2;
            }
            case 12:
            case 13:
            case 14:
            case 15: {
                return 3;
            }
        }
        return 0;
    }

    /**
     * Метод, решающий судоку через проверку строк
     */
    private static void testByLines() {
        int cell = 0;
        for (int index = 0; index < 4; index++) {
            for (int value = cell; value < cell + 4; value++) {
                solution(index, value);
            }
            cell += 4;
        }
    }


    /**
     * Метод, решающий судоку через проверку столбцов
     */
    private static void testByColumns() {
        int cell = 0;
        for (int index = 4; index < 8; index++) {
            for (int value = cell; value < cell + 12; value += 4) {
                solution(index, value);
            }
            cell++;
        }
    }

    /**
     * Метод, решающий судоку через проверку квадратов
     */
    private static void testBySquares() {
        int[] valuesOfSquares = {0, 1, 4, 5};
        for (int index = 8; index < 12; index++) {
            for (int value : valuesOfSquares) {
                solution(index, value);
            }
            for (int value = 0; value < valuesOfSquares.length; value++) {
                if (index == 9) {
                    valuesOfSquares[value] += 6;
                } else {
                    valuesOfSquares[value] += 2;
                }
            }
        }
    }

    /**
     * Метод, который тут, собственно, главный и он-то все и решает
     *
     * @param index - номер суммы в массивчике
     * @param value - значение ячейки судоку
     */
    private static void solution(int index, int value) {
        if ((sum[index][0] == 3) && (arrayOfPoints.contains(value))) {
            int x = 10 - sum[index][1];
            table[value] = String.valueOf(x).charAt(0);
            sum[line(value)][0]++;
            sum[line(value)][1] += x;
            sum[column(value)][0]++;
            sum[column(value)][1] += x;
            sum[square(value)][0]++;
            sum[square(value)][1] += x;
            arrayOfPoints.remove(arrayOfPoints.indexOf(value));
        }
    }

    /**
     * Метод, который записывает готовое судоку в файл
     */
    private static void writeResult() {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("output.txt"))) {
            bufferedWriter.write(table[0] + "\t" + table[1] + "\t" + table[2] + "\t" + table[3] + "\n" +
                    table[4] + "\t" + table[5] + "\t" + table[6] + "\t" + table[7] + "\n" +
                    table[8] + "\t" + table[9] + "\t" + table[10] + "\t" + table[11] + "\n" +
                    table[12] + "\t" + table[13] + "\t" + table[14] + "\t" + table[15]);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

