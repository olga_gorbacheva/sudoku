package ru.god.sum;

import java.io.*;
import java.util.ArrayList;

public class NewSolution {
    private static char[] table = readArray();
    private static ArrayList<Integer> arrayOfPoints = new ArrayList<>();
    private static boolean[][] sum = {{false, false, false, false},
            {false, false, false, false},
            {false, false, false, false},
            {false, false, false, false},
            {false, false, false, false},
            {false, false, false, false},
            {false, false, false, false},
            {false, false, false, false},
            {false, false, false, false},
            {false, false, false, false},
            {false, false, false, false},
            {false, false, false, false}};

    public static void main(String[] args) {
        calc();
        solution();
        writeResult();
    }

    private static char[] readArray() {
        char[] array = new char[16];
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\ru\\god\\sum\\input.txt"))) {
            String string;
            for (int index = 0; index < 16; ) {
                string = bufferedReader.readLine();
                for (int i = 0; i < 4; i++) {
                    array[index] = string.charAt(i);
                    index++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return array;
    }

    private static void calc() {
        for (int i = 0; i < 16; i++) {
            switch (table[i]) {
                case '.': {
                    arrayOfPoints.add(i);
                    break;
                }
                default: {
                    sum[line(i)][table[i] - 49] = true;
                    sum[column(i)][table[i] - 49] = true;
                    sum[square(i)][table[i] - 49] = true;
                    break;
                }
            }
        }
    }

    private static void solution() {
        int value;
        int a;
        int b;
        int c;
        while (!arrayOfPoints.isEmpty()) {
            for (int i = 0; i < arrayOfPoints.size(); i++) {
                value = arrayOfPoints.get(i);
                for (int j = 0; j < 4; j++) {
                    a = 0;
                    b = 1;
                    c = 3;
                    switch (j) {
                        case 0: {
                            a = 2;
                            break;
                        }
                        case 1: {
                            b = 2;
                            break;
                        }
                        case 3: {
                            c = 2;
                            break;
                        }
                    }
                    if ((sum[line(value)][a] || sum[column(value)][a] || sum[square(value)][a]) &&
                            (sum[line(value)][b] || sum[column(value)][b] || sum[square(value)][b]) &&
                            (sum[line(value)][c] || sum[column(value)][c] || sum[square(value)][c])) {
                        table[value] = String.valueOf(j + 1).charAt(0);
                        sum[line(value)][j] = true;
                        sum[column(value)][j] = true;
                        sum[square(value)][j] = true;
                        arrayOfPoints.remove(i--);
                        break;
                    }
                }
            }
        }
    }

    /**
     * Метод, определяющий, к какому квадрату относится ячейка
     *
     * @param cell - номер ячейки
     * @return номер квадрата
     */
    private static int square(int cell) {
        switch (cell) {
            case 0:
            case 1:
            case 4:
            case 5: {
                return 8;
            }
            case 2:
            case 3:
            case 6:
            case 7: {
                return 9;
            }
            case 8:
            case 9:
            case 12:
            case 13: {
                return 10;
            }
            case 10:
            case 11:
            case 14:
            case 15: {
                return 11;
            }
        }
        return 0;
    }

    /**
     * Метод, определяющий, к какому столбцу относится ячейка
     *
     * @param cell - номер ячейки
     * @return номер столбца
     */
    private static int column(int cell) {
        switch (cell) {
            case 0:
            case 4:
            case 8:
            case 12: {
                return 4;
            }
            case 1:
            case 5:
            case 9:
            case 13: {
                return 5;
            }
            case 2:
            case 6:
            case 10:
            case 14: {
                return 6;
            }
            case 3:
            case 7:
            case 11:
            case 15: {
                return 7;
            }
        }
        return 0;
    }

    /**
     * Метод, определяющий, к какой строке относится ячейка
     *
     * @param cell - номер ячейки
     * @return номер строки
     */
    private static int line(int cell) {
        switch (cell) {
            case 0:
            case 1:
            case 2:
            case 3: {
                return 0;
            }
            case 4:
            case 5:
            case 6:
            case 7: {
                return 1;
            }
            case 8:
            case 9:
            case 10:
            case 11: {
                return 2;
            }
            case 12:
            case 13:
            case 14:
            case 15: {
                return 3;
            }
        }
        return 0;
    }

    private static void writeResult() {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("output.txt"))) {
            for (int index = 0; index < 16; index += 4) {
                bufferedWriter.write(table[index] + "\t" + table[index + 1] + "\t" + table[index + 2] + "\t" +
                        table[index + 3] + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}/*
 &&
         !sum[line(value)][j] && !sum[column(value)][j] && !sum[square(value)][j]*/